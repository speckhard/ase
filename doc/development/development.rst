.. _devel:

===========
Development
===========

As a developer, you should subscribe to the ASE :ref:`mail list`.


Development topics:

.. toctree::

    contribute
    python_codingstandard
    writing_documentation_ase
    writing_changelog
    calculators
    making_movies
    newrelease
    tests
    bugs
    licenseinfo
    translate
    proposals/proposals
